import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class BatsmanTest {

    @Test
    void whenBatsManIsAHitterBatsmanPermittedScoresShouldBeZeroOrFourOrSix(){
        Batsman batsman = new Hitter();
        List<Integer> possibleScores = new ArrayList<>(Arrays.asList(0,4,6));
        assertTrue(possibleScores.contains(batsman.bat()));

    }
    @Test
    void whenBatsManIsANormalBatsmanPermittedScoresShouldBeZeroOrFourOrSix(){
        Batsman batsman = new Normal();
        List<Integer> possibleScores = new ArrayList<>(Arrays.asList(0,1,2,3,4,5,6));


        assertTrue(possibleScores.contains(batsman.bat()));

    }

    @Test
    void whenBatsManIsADefensiveBatsmanPermittedScoresShouldBeZeroOrFourOrSix(){
        Batsman batsman = new Defensive();
        List<Integer> possibleScores = new ArrayList<>(Arrays.asList(0,1,2,3));

        assertTrue(possibleScores.contains(batsman.bat()));

    }
}
