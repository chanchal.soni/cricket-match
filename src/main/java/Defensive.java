import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Defensive implements Batsman{
    @Override
    public int bat() {
        Random random = new Random();
        List<Integer> possibleScores = new ArrayList<>(Arrays.asList(0,1,2,3));
        return possibleScores.get(random.nextInt(possibleScores.size()));
    }
}
