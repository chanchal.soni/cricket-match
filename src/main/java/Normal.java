import java.util.Random;

public class Normal implements Batsman{
    private static final int UPPERBOUND = 6;

    @Override
    public int bat() {
        return new Random().nextInt(UPPERBOUND);
    }
}
