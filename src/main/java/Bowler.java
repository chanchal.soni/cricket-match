import java.util.Random;

class Bowler{
    private static final int UPPERBOUND = 6;
    public final BowlerType bowlerType;

    Bowler(BowlerType bowlerType){
        this.bowlerType = bowlerType;
    }

    int bowl(){
        return new Random().nextInt(UPPERBOUND);

    }
}